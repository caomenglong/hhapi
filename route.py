#!/usr/bin/env python
# -*- coding: utf-8 -*-
from resources import index,foo

#项目URL前缀
urlprefix="/api/v1.0/"

def init(api):
    # 路由规则
    api.add_resource(index.Info, '/', urlprefix)
    api.add_resource(foo.Foo, urlprefix + "foo/<int:id>",urlprefix + "foo")
    api.add_resource(foo.Foos, urlprefix + "foos")