#!/usr/bin/env python
# -*- coding: utf-8 -*-
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, String,Integer
# 创建对象的基类:
from base import BaseModel

Base = declarative_base()

# 定义User对象:
class AppKey(Base,BaseModel):
    # 表的名字:
    __tablename__ = 'hh_appkey'

    # 表的结构:
    id = Column(Integer, primary_key=True)
    appkey = Column(String(200))
    securitykey = Column(String(200))

    def __init__(self, appkey, securitykey):
        self.appkey = appkey
        self.securitykey = securitykey


