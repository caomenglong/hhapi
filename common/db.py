#!/usr/bin/env python
# -*- coding: utf-8 -*-
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

# 初始化数据库连接:
engine = create_engine('mysql+pymysql://root:87252798@127.0.0.1/hhapi')
# 创建DBSession类型:
DBSession = sessionmaker(bind=engine)


def getDBSession():
    return DBSession()
def getSessionMaker():
    return sessionmaker(bind=engine)
