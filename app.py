#!/usr/bin/env python
# -*- coding: utf-8 -*-
from flask import Flask
from flask_restful import Api
import route

app = Flask(__name__)
app.debug=True
api = Api(app)

#加载路由配置
route.init(api)

if __name__ == '__main__':
    app.run()
