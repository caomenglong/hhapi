from flask_restful import Resource
from base import BaseResource


class Info(BaseResource):
    def get(self):
        return {'message': 'Server is Online'}