#!/usr/bin/env python
# -*- coding: utf-8 -*-
from base import BaseResource
from flask import request
from models.appkey import AppKey
import common.util
import common.db
import json

class Foo(BaseResource):
    #读取一个资源
    def get(self,id):
        session = common.db.getDBSession()
        appkey=session.query(AppKey).filter_by(id=id).first()
        session.close()
        result= common.util.to_json(appkey)
        return result
    #添加资源
    def post(self):
        appkeystr=request.form['appkey']
        securitykeystr = request.form['securitykey']
        appkey =AppKey(appkey=appkeystr,securitykey=securitykeystr)
        session = common.db.getDBSession()
        session.add(appkey)
        session.commit()
        d = appkey.serialize()
        session.close()
        return d

    #修改资源
    def put(self,id):

        pass

    #删除资源
    def delete(self):
        pass



class Foos(BaseResource):
    #获取全部资源
    def get(self):
        session =common.db.getDBSession()
        appkeylist =session.query(AppKey).all()
        session.close()
        result = common.util.to_json_list(appkeylist)
        return result